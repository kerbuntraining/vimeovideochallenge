import Vue from 'vue'
import Router from 'vue-router'
import MainPage from './components/MainPage.vue'
import Video from './components/Video.vue'
Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'mainPage',
            component: MainPage
        },
        {
            path: '/video',
            name: 'video',
            component: Video
        }
    ]
})